define([
	"jquery",
	"backbone",
	"views/homeView",
	"views/mainPanel",
	"views/page1View",
	"views/page2View",
], function(
	$,
	Backbone,
	HomeView,
	MainPanel,
	Page1View,
	Page2View ){

	var AppRouter = Backbone.Router.extend({
		routes: {
			"": "home",
			"home": "home",
			"page1": "page1",
			"page2": "page2"
		},


		initialize: function(){
			console.log( "router.js [initialize]: start" );

			/* Add the Main Panel to the DOM */
			var panel = ( new MainPanel() ).render();
			$( panel.$el ).appendTo("body");
			$("#mainPanel").panel().enhanceWithin();
		},


		home: function() {
			this.changePage( new HomeView(), "slidedown" );
		},


		page1: function() {
			this.changePage( new Page1View() );
		},


		page2: function() {
			this.changePage( new Page2View() );
		},


		changePage: function( view, transition ){
			console.log( "router.js [changePage]: start" );

			if( !transition ){
				transition = "slide";
			}

			$('div[data-role="page"]').bind("pagehide", function(event, ui) {
				console.log( "Hiding page" );
				$(event.currentTarget).remove();
			});

			var page = view.render();

			$("body").append( page.$el );
			$("body").pagecontainer( "change", $(page.el), { transition: transition, changeHash:false });
		}

	});

	return AppRouter;
});