requirejs.config({
	// By default load any module IDs from js/lib
	baseUrl: "js",

	// except, if the module ID starts with "app", /load it from the js/app directory. paths
	// config is relative to the baseUrl, and never includes a ".js" extension since
	// the paths config could be for a directory.
	paths: {
		jquery: "lib/jquery/jquery-1.10.2.min",
		"jquery.mobile": "lib/jquery/jquery.mobile-1.4.0.min",
		"jquery.mobile.config": "jquery.mobile.config",
		backbone: "lib/backbone-1.1.0.min",
		underscore: "lib/underscore-1.5.2.min",
		handlebars: "lib/handlebars-v1.3.0",
		text: "lib/text",
		router: "router"
	},

	// modules who aren't defined using the "define()" method - ie. standard JS files
	shim: {
		"backbone": {
			deps: [ "underscore", "jquery" ],
			exports: "Backbone"
		},
		"jquery.mobile.config": {
			deps: [ "jquery" ],
			exports: "jqmconfig"
		},
		"jquery.mobile": {
			deps: [ "jquery", "jquery.mobile.config" ],
			exports: "jqm"
		},
		"underscore": {
			exports: "_"
		},
		"handlebars": {
			exports: "Handlebars"
		}
	}
});


// Start the main app logic.
requirejs( [ "jquery", "backbone", "handlebars", "router", "text", "jquery.mobile" ],
	function( $, Backbone, Handlebars, Router, text, jqm ) {

		var router = new Router();

		Backbone.history.start();
	}
);