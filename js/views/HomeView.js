define([
	"jquery",
	"backbone",
	"handlebars",
	"text!templates/homeView.html",
	"views/header_defaultView",
	"views/footer_defaultView"
], function(
	$,
	Backbone,
	Handlebars,
	Template,
	HeaderView,
	FooterView
){
	var HomeView = Backbone.View.extend({

		tagName: "div",

		events: {
			// no events
		},

		attributes: {
			"data-role": "page",
			"data-title": "Home View"
		},


		initialize: function(){
			console.log( "HomeView.js [initialize]: start" );
		},


		render: function(){
			console.log( "HomeView.js [render]: start" );

			var compiledTemplate = Handlebars.compile( Template );
			var dModel = {
				first: "Brad",
				last: "Bisinger"
			}


			$(this.el).html( compiledTemplate(dModel) );
			
			var header = ( new HeaderView({"title":"Home View Header"}) ).render();
			$( header.$el ).appendTo( this.el );

			var footer = ( new FooterView({"title":"Home View Footer"}) ).render();
			$( footer.$el ).appendTo( this.el );

			return this;
		}
	});

	return HomeView;
});