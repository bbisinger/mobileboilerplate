define([
	"jquery",
	"backbone",
	"handlebars",
	"text!templates/mainPanel.html",
	"app/panelCtrl"
], function(
	$,
	Backbone,
	Handlebars,
	Template,
	PanelCtrl
){
	var MainPanel = Backbone.View.extend({

		tagName: "div",

		attributes: {
			"data-role": "panel",
			"data-position": "left",
			"data-display": "push",
			"data-theme": "a",
			"id": "mainPanel"
		},

		events: {
			"click button#btnClosePanel": "closePanel"
		},


		initialize: function(){
			console.log( "mainPanel.js [initialize]: start" );
		},


		render: function(){
			console.log( "mainPanel.js [render]: start" );
			
			var compiledTemplate = Handlebars.compile( Template );

			var dModel = {
				first: "Brad",
				last: "Bisinger"
			}

			$(this.el).html( compiledTemplate(dModel) );

			return this;
		},


		closePanel: function(){
			PanelCtrl.closePanel();
		}

	});

	return MainPanel;
});