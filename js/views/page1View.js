define([
	"jquery",
	"backbone",
	"handlebars",
	"text!templates/page1View.html",
	"views/header_defaultView",
	"views/footer_defaultView"
], function(
	$,
	Backbone,
	Handlebars,
	Template,
	HeaderView,
	FooterView
){
	var Page1View = Backbone.View.extend({

		tagName: "div",

		events: {
			// no events
		},

		attributes: {
			"data-role": "page",
			"data-title": "Page 1 View"
		},


		initialize: function(){
			console.log( "Page1View.js [initialize]: start" );
		},


		render: function(){
			console.log( "Page1View.js [render]: start" );
			
			var compiledTemplate = Handlebars.compile( Template );
			var dModel = {
				first: "Katherine",
				last: "Bisinger"
			}

			$(this.el).html( compiledTemplate(dModel) );
			
			var header = ( new HeaderView({"title":"View 1 Header"}) ).render();
			$( header.$el ).appendTo( this.el );

			var footer = ( new FooterView({"title":"View 1 Footer"}) ).render();
			$( footer.$el ).appendTo( this.el );

			return this;
		}
	});

	return Page1View;
});