define([
	"jquery",
	"backbone",
	"handlebars",
	"text!templates/footer_defaultView.html"
], function(
	$,
	Backbone,
	Handlebars,
	Template
){
	var FooterDefaultView = Backbone.View.extend({

		tagName: "div",

		events: {
			// no events
		},

		attributes: {
			"data-role": "footer",
			"data-position": "fixed"
		},

		title: "",


		initialize: function( options ){
			console.log( "footer_defaultView.js [initialize]: start" );

			if( !options ){
				options = {};
			}

			if( options.hasOwnProperty("title") ){
				this.title = options.title;
			}
		},


		render: function(){
			console.log( "footer_defaultView.js [render]: start" );

			var compiledTemplate = Handlebars.compile( Template );
			var fmodel = {
				title: this.title
			}

			$(this.el).html( compiledTemplate(fmodel) );

			return this;
		}
	});

	return FooterDefaultView;
});