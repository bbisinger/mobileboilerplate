define([
	"jquery",
	"backbone",
	"handlebars",
	"text!templates/header_defaultView.html",
	"app/panelCtrl"
], function(
	$,
	Backbone,
	Handlebars,
	Template,
	panelCtrl
){
	var HeaderDefaultView = Backbone.View.extend({

		tagName: "div",

		events: {
			"click a#btnOpenPanel": "openPanel"
		},

		attributes: {
			"data-role": "header",
			"data-position": "fixed"
		},


		title: "",


		initialize: function( options ){
			console.log( "header_defaultView.js [initialize]: start" );

			if( !options ){
				options = {};
			}

			if( options.hasOwnProperty("title") ){
				this.title = options.title;
			}
		},


		render: function(){
			console.log( "header_defaultView.js [render]: start" );

			var compiledTemplate = Handlebars.compile( Template );
			var hmodel = {
				title: this.title
			}

			$(this.el).html( compiledTemplate(hmodel) );

			return this;
		},


		openPanel: function(){
			console.log( "header_defaultView.js [openPanel]: start" );
			panelCtrl.openPanel();
		}
	});

	return HeaderDefaultView;
});