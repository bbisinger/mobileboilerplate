define([
	"jquery",
	"backbone",
	"handlebars",
	"text!templates/page2View.html",
	"views/header_defaultView",
	"views/footer_defaultView"
], function(
	$,
	Backbone,
	Handlebars,
	Template,
	HeaderView,
	FooterView
){
	var Page2View = Backbone.View.extend({

		tagName: "div",

		events: {
			// no events
		},

		attributes: {
			"data-role": "page",
			"data-title": "Page 1 View"
		},


		initialize: function(){
			console.log( "Page2View.js [initialize]: start" );
		},


		render: function(){
			console.log( "Page2View.js [render]: start" );

			var compiledTemplate = Handlebars.compile( Template );
			var dModel = {
				text: "This page has no header or footer data passed in"
			}

			$(this.el).html( compiledTemplate(dModel) );
			
			var header = ( new HeaderView() ).render();
			$( header.$el ).appendTo( this.el );

			var footer = ( new FooterView() ).render();
			$( footer.$el ).appendTo( this.el );

			return this;
		}
	});

	return Page2View;
});