define([
	"jquery",
	"backbone",
	"handlebars"
], function(
	$,
	Backbone,
	Handlebars
){
	var PanelController = {

		openPanel: function(){
			console.log( "panelCtrl.js [openPanel]: start" );
			$("#mainPanel").panel("open");
		},


		closePanel: function(){
			console.log( "panelCtrl.js [closePanel]: start" );
			$("#mainPanel").panel("close");
		}
	}

	return PanelController;
});